#!/bin/bash
 
# as a example we have used \n as a new line, \x40 is hex value for @
# and  is octal value for .
echo $'web: www.linuxconfig.org\nemail: web\x40linuxconfigorg\n'

manager='Ella Arts\v\tVery reliable!'
club="Pingpong Club\v\tLots of people enjoyed!"

str="web: www.linuxconfig.org\nemail: web\x40linuxconfigorg\nClub: $club\nManager: $manager"
echo "${str@E}"
echo *****use -e option:******
echo -e $str