#!/bin/bash
# to store multiple lines string into a variable
#use $() and here-document
text=$(cat <<-EOT
line 1
line 2
    some other text
    more text
EOT
)

echo "$text"

#use read and here-document
echo -e "\n***use read***\n"
read -r -d '' text <<-EOT
line 1 from read
line 2
    some other text
    more text
EOT

echo "$text"