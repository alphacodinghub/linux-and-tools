#!/bin/bash

set -e

## to generate wp-config.php
db_host='myHost.com'
db_name='myDB'
db_charset='utf8'
table_prefix='wp_'
db_user='george'
db_password='secret'

php="<?php \n"
php="${php}\$table_prefix = '$table_prefix'\n\n"
php="${php}define('DB_HOST', '$db_host')\n"
php="${php}define('DB_NAME', '$db_name')\n"
php="${php}define('DB_USER', '$db_user')\n"
php="${php}define('DB_PASSWORD', '$db_password')\n"
php="${php}define('DB_CHARSET', '$db_charset')\n\n"

str="$(cat <<-EOT
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
    $_SERVER['HTTPS'] = 'on';
}

if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/');
}

require_once(ABSPATH . 'wp-secrets.php');

require_once(ABSPATH . 'wp-settings.php');
EOT
)"

echo -e $php 
echo "$str" 
