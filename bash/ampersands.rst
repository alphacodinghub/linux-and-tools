.. _amp-file:

Ampersands and File Descriptors in Bash
=========================================
**Ref**:

* `Ampersands and File Descriptors <https://www.linux.com/training-tutorials/ampersands-and-file-descriptors-bash/>`_

.. code-block:: bash
    :emphasize-lines: 1,3,5
    :linenos:

    ls > out.txt
    # same as above, '1' stands for 'stdout':
    ls 1> out.txt
    # '2' stands for 'stderr' in the below
    ls 2> err.txt

.. tip:: ``0<``: stdin, ``1>``: stdout, ``2>``: stderr, ``&>``: both stdout and stderr, ``2>&1``: stderr pipes to stdin.

More examples:

.. code-block:: bash
    :emphasize-lines: 11,15
    :linenos:

    # results save to services.txt, errors display on the terminal
    find /etc -iname "*.service" 1> services.txt

    # errors save to services.txt, results display on the terminal
    find /etc -iname "*.service" 2> services.txt

    # both results and errors save to services.txt
    find /etc -iname "*.service" &> services.txt

    # results save to services.txt, errors display on the terminal
    find /etc -iname "*.service" 1> services.txt 2>&1
    # This pipes stderr to stdout and stdout is piped to a file, services.txt.

    # results save to services.txt, errors display on the terminal
    find /etc -iname "*.service" 2>&1 1>services.txt
    # when errors are redirected to stdout (2>&1), stdout is still pointing to terminal.

.. important:: Please note the differece between Line 11 and 15.



