..  _bash-operators:

Bash operators
===============
Numeric and String Comparisons
-------------------------------

.. csv-table:: Bash Shell Numeric and String Comparisons
    :header: Description,	Numeric Comparison,	String Comparison
    :widths: 30, 20, 20

    less than,	-lt,	<
    greater than,	-gt,	>
    equal,	-eq,	=
    not equal,	-ne,	!=
    less or equal,	-le,	N/A
    greater or equal,	-ge,	N/A
    Shell comparison example:,	[ 100 -eq 50 ]; echo $?,	[ "GNU" = "UNIX" ]; echo $?

Bash File Testing
----------------------

.. csv-table:: Bash File Testing
    :widths: 25, 40

    -b filename,	Block special file
    -c filename,	Special character file
    -d directoryname,	Check for directory existence
    -e filename,	Check for file existence
    -f filename,	Check for regular file existence not a directory
    -G filename,	Check if file exists and is owned by effective group ID.
    -g filename,	true if file exists and is set-group-id.
    -k filename,	Sticky bit
    -L filename,	Symbolic link
    -O filename,	True if file exists and is owned by the effective user id.
    -r filename,	Check if file is a readable
    -S filename,	Check if file is socket
    -s filename,	Check if file is nonzero size
    -u filename,	Check if file set-ser-id bit is set
    -w filename,	Check if file is writable
    -x filename,	Check if file is executable

.. code-block:: bash
    :linenos:

    #!/bin/bash
    file="./file"
    if [ -e $file ]; then
        echo "File exists"
    else 
        echo "File does not exists"
    fi 