.. _colon-in-bash:

Colon (:) in Bash
===================
Bash and Sh both use colons ``:`` in a few contexts:

* Seperator, e.g. $PATH="$PATH:/usr/bin:/opt/app"
* Modifier /expansion, e.g. ${a:=foo}
* Null operator, e.g. "while :"

Null operator
---------------

You'll also see the null operator usage in "if-then" sections:

.. code-block:: bash
    :emphasize-lines: 21
    :linenos:

    if [ "$T1" = "$T2" ]
    then
        :
    else
        echo "Nope"
    fi           
    
In that case, the ":" is just a **"do-nothing"** place holder.

The use that probably causes the most head-scratching is when it appears at the beginning of a line::

    : > somefile
 
What does that mean? It simply clears out "somefile" or creates an empty "somefile" if it didn't exist already. In most shells, you wouldn't need the ":" at all - a '> somefile' would work just as well. But that wouldn't work in Csh - you'd get "Invalid null command".

You could 'cat /dev/null > somefile' but that's a lot of typing and is very Unix specific. You could 'echo -n > somefile' on many systems, but on a few you might need 'echo "\c"' instead. Using ': > somefile' is short, not Unix specific and avoids variances in 'echo'.

Another place you might see it is with conditional variable setting. For example, say we want to set xx to "foo" but only if it isn't already set. We can use use '${xx:="foo"}' as a shorthand way to avoid "if" or "case" blocks, but that has a side effect::

    ${xx:="foo"}
    -bash: foo: command not found
 
You could redirect errout to stop that complaint, but what if 'foo' were a real command? It would be executed, and that might not be what you want. You can avoid that by the leading ":" again::

    : ${xx:="foo"}
 
So that's why those leading colons are often found in shell scripts.

Shell Parameter Expansion ${parameter: ...}
-----------------------------------------------
**${parameter:-word}**

    If parameter is **unset or null**, the expansion of word is substituted. Otherwise, the value of parameter is substituted::

        a=null
        echo ${a:-hello}
        echo ${b:-world}

    Output::

        hello
        world        

**${parameter:=word}**

    If parameter is **unset**, the expansion of word is assigned to parameter. The value of parameter is then substituted. Positional parameters and special parameters may not be assigned to in this way::

        a=null
        echo ${a:=hello}
        echo ${b:=world}

    Output::

        null
        world     

**${parameter:?word}**

    If parameter is null or unset, the expansion of word (or a message to that effect if word is not present) is written to the standard error and the shell, if it is not interactive, exits. Otherwise, the value of parameter is substituted.

**${parameter:+word}**

    If parameter is null or unset, nothing is substituted, otherwise the expansion of word is substituted.

**${parameter:offset}**

**${parameter:offset:length}**

    This is referred to as Substring Expansion. It expands to up to length characters of the value of parameter starting at the character specified by offset. If parameter is ‘@’, an indexed array subscripted by ‘@’ or ‘*’, or an associative array name, the results differ as described below. If length is omitted, it expands to the substring of the value of parameter starting at the character specified by offset and extending to the end of the value. length and offset are arithmetic expressions.

    ::

        $ string=01234567890abcdefgh
        $ echo ${string:7}
        7890abcdefgh
        $ echo ${string:7:0}

        $ echo ${string:7:2}
        78
        $ echo ${string:7:-2}
        7890abcdef
        $ echo ${string: -7}
        bcdefgh
        $ echo ${string: -7:0}

        $ echo ${string: -7:2}
        bc
        $ echo ${string: -7:-2}
        bcdef
        $ set -- 01234567890abcdefgh
        $ echo ${1:7}
        7890abcdefgh
        $ echo ${1:7:0}

        $ echo ${1:7:2}
        78

    If parameter is ‘@’, the result is length positional parameters beginning at offset. A negative offset is taken relative to one greater than the greatest positional parameter, so an offset of -1 evaluates to the last positional parameter. It is an expansion error if length evaluates to a number less than zero.

    The following examples illustrate substring expansion using positional parameters::

        $ set -- 1 2 3 4 5 6 7 8 9 0 a b c d e f g h
        $ echo ${@:7}
        7 8 9 0 a b c d e f g h
        $ echo ${@:7:0}

        $ echo ${@:7:2}
        7 8
        $ echo ${@:7:-2}
        bash: -2: substring expression < 0
        $ echo ${@: -7:2}
        b c
        $ echo ${@:0}
        ./bash 1 2 3 4 5 6 7 8 9 0 a b c d e f g h
        $ echo ${@:0:2}
        ./bash 1
        $ echo ${@: -7:0}

    If parameter is an indexed array name subscripted by ‘@’ or ‘*’, the result is the length members of the array beginning with ${parameter[offset]}. A negative offset is taken relative to one greater than the maximum index of the specified array. It is an expansion error if length evaluates to a number less than zero.

    These examples show how you can use substring expansion with indexed arrays::

        $ array=(0 1 2 3 4 5 6 7 8 9 0 a b c d e f g h)
        $ echo ${array[@]:7}
        7 8 9 0 a b c d e f g h
        $ echo ${array[@]:7:2}
        7 8

**${#parameter}**

    The length in characters of the expanded value of parameter is substituted. If parameter is ‘*’ or ‘@’, the value substituted is the number of positional parameters. If parameter is an array name subscripted by ‘*’ or ‘@’, the value substituted is the number of elements in the array. If parameter is an indexed array name subscripted by a negative number, that number is interpreted as relative to one greater than the maximum index of parameter, so negative indices count back from the end of the array, and an index of -1 references the last element.

**${parameter#word}**

**${parameter##word}**

    The word is expanded to produce a pattern and matched according to the rules described below (see Pattern Matching). If the pattern matches the beginning of the expanded value of parameter, then the result of the expansion is the expanded value of parameter with the shortest matching pattern (the ‘#’ case) or the longest matching pattern (the ‘##’ case) deleted. If parameter is ‘@’ or ‘*’, the pattern removal operation is applied to each positional parameter in turn, and the expansion is the resultant list. If parameter is an array variable subscripted with ‘@’ or ‘*’, the pattern removal operation is applied to each member of the array in turn, and the expansion is the resultant list.

**${parameter%word}**

**${parameter%%word}**

    The word is expanded to produce a pattern and matched according to the rules described below (see Pattern Matching). If the pattern matches a trailing portion of the expanded value of parameter, then the result of the expansion is the value of parameter with the shortest matching pattern (the ‘%’ case) or the longest matching pattern (the ‘%%’ case) deleted. If parameter is ‘@’ or ‘*’, the pattern removal operation is applied to each positional parameter in turn, and the expansion is the resultant list. If parameter is an array variable subscripted with ‘@’ or ‘*’, the pattern removal operation is applied to each member of the array in turn, and the expansion is the resultant list.

**${parameter/pattern/string}**

    The pattern is expanded to produce a pattern just as in filename expansion. Parameter is expanded and the longest match of pattern against its value is replaced with string. The match is performed according to the rules described below (see Pattern Matching). If pattern begins with ‘/’, all matches of pattern are replaced with string. Normally only the first match is replaced. If pattern begins with ‘#’, it must match at the beginning of the expanded value of parameter. If pattern begins with ‘%’, it must match at the end of the expanded value of parameter. If string is null, matches of pattern are deleted and the / following pattern may be omitted. If the nocasematch shell option (see the description of shopt in The Shopt Builtin) is enabled, the match is performed without regard to the case of alphabetic characters. If parameter is ‘@’ or ‘*’, the substitution operation is applied to each positional parameter in turn, and the expansion is the resultant list. If parameter is an array variable subscripted with ‘@’ or ‘*’, the substitution operation is applied to each member of the array in turn, and the expansion is the resultant list.        