.. _bash:

Bash Tutorials
===============

.. toctree::
    :maxdepth: 2
    :glob:

    *


**Refs**:

* `Bash Reference Manual <https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#Bash-Conditional-Expressions>`_