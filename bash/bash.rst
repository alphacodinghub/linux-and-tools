.. _bash-elements:

Bash Element
===================

**Refs**:
* `Bash scripting Tutorial <https://linuxconfig.org/bash-scripting-tutorial#h17-bash-quoting-with-ansi-c-style>`_
* `Bash Scripting Tutorial for Beginners <https://linuxconfig.org/bash-scripting-tutorial-for-beginners#h18-2-expr-command>`_
* `WordPress Docker init Scripts <https://github.com/docker-library/wordpress/blob/d447e82d5e847f0b06d53214b0119f9f9f67340a/php7.2/apache/docker-entrypoint.sh>`_

Global vs. Local variables
-----------------------------

.. literalinclude:: code/bash/variables.sh
    :language: bash
    :emphasize-lines: 4,8
    :linenos:

Output::

    bash$ ./variables.sh 
    global variable
    local variable
    global variable

Passing arguments to the bash script
-------------------------------------

.. literalinclude:: code/bash/args.sh
    :language: bash
    :emphasize-lines: 5,8,13
    :linenos:

Output::

    bash$ ./args.sh a b c
    ./args.sh a b c  -> echo $0 $1 $2 $3
    a b c  -> args=("$@"); echo ${args[0]} ${args[1]} ${args[2]}
    a b c  -> echo $@
    Number of arguments passed: 3  -> echo Number of arguments passed: $#

Executing shell commands using backticks and $()
------------------------------------------------------------------

.. literalinclude:: code/bash/run-commands.sh
    :language: bash
    :emphasize-lines: 3,5,7
    :linenos:

Output::

    bash$ ./run-commands.sh 
    GNU/Linux
    GNU/Linux
    uname -o

Reading User Input
--------------------

.. literalinclude:: code/bash/read.sh
    :language: bash
    :emphasize-lines: 3,15
    :linenos:

Output::

    bash$ ./read.sh 
    Hi, please type the word: hello
    The word you entered is: hello
    Can you please enter two words? 
    hello world
    Here is your input: "hello" "world"
    How do you feel about bash scripting? 
    Great!
    You said Great!, I'm glad to hear that! 
    What are your favorite colours ? 
    red yello blue
    My favorite colours are also red, yello and blue:-)

Bash Trap Command
------------------

.. literalinclude:: code/bash/bash-trap.sh
    :language: bash
    :emphasize-lines: 3,8,13
    :linenos:

Arrays
------------
**Declare simple bash array**

.. literalinclude:: code/bash/arrays.sh
    :language: bash
    :emphasize-lines: 5-7
    :linenos:

Output: 
Please note Line 3 & 4 in the below output.

.. code-block:: shell
    :emphasize-lines: 3-4
    :linenos:

    bash$ ./arrays.sh 
    Debian Linux[@] --> echo $ARRAY[@)
    Debian Linux Redhat Linux Ubuntu Linux --> echo ${ARRAY[@]}
    Debian Linux
    Redhat Linux
    Ubuntu
    Linux

**Read file into bash array**



Bash quoting with ANSI-C style
-------------------------------
There is also another type of quoting and that is ANSI-C. In this type of quoting characters escaped with "\" will gain special meaning according to the ANSI-C standard.

.. csv-table:: CSV Table
    :widths: 5,35,5,35

    \\a,	alert (bell),	\\b,	backspace
    \\e,	an escape character,	\\f,	form feed
    \\n,	newline,	\\r,	carriage return
    \\t,	horizontal tab,	\\v,	vertical tab
    \\ \\,	backslash,	\\`,	single quote
    \\nnn,	octal value of characters (see `ASCII table <http://www.asciitable.com/>`_),	\\xnn,	hexadecimal value of characters (see `ASCII table <http://www.asciitable.com/>`_)


Examples:

.. literalinclude:: code/bash/ansi-c-style.sh
    :language: bash
    :emphasize-lines: 5,10-11,13
    :linenos:

Output:

.. code-block:: bash
    :linenos:

    bash$ ./ansi-c-style.sh 
    web: www.linuxconfig.org
    email: web@linuxconfigorg

    web: www.linuxconfig.org
    email: web@linuxconfigorg
    Club: Pingpong Club
                            Lots of people enjoyed!
    Manager: Ella Arts
                            Very reliable!
    *****use -e option:******
    web: www.linuxconfig.org
    email: web@linuxconfigorg
    Club: Pingpong Club
                            Lots of people enjoyed!
    Manager: Ella Arts
                            Very reliable!