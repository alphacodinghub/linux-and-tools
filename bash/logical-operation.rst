.. _logical-op:

Logical Operation in Bash
===========================

**Refs**:

* `Logical & in Bash <https://www.linux.com/training-tutorials/logical-ampersand-bash/>`_

& is a Bitwise Operator
-------------------------
In Bash, you use & as the AND operator and | as the OR operator:

AND

::
    0 & 0 = 0
    0 & 1 = 0
    1 & 0 = 0
    1 & 1 = 1

OR

::

    0 | 0 = 0

    0 | 1 = 1

    1 | 0 = 1

    1 | 1 = 1

You can test this by ANDing any two numbers and outputting the result with echo::

    $ echo $(( 2 & 3 )) # 00000010 AND 00000011 = 00000010
    2

    $ echo $(( 120 & 97 )) # 01111000 AND 01100001 = 01100000
    96

The same goes for OR (|)::

    $ echo $(( 2 | 3 )) # 00000010 OR 00000011 = 00000011
    3

    $ echo $(( 120 | 97 )) # 01111000 OR 01100001 = 01111001
    121

Three things about this:

* You use (( ... )) to tell Bash that what goes between the double brackets is some sort of arithmetic or logical operation. (( 2 + 2 )), (( 5 % 2 )) (% being the modulo operator) and ((( 5 % 2 ) + 1)) (equals 3) will all work.
* Like with variables, $ extracts the value so you can use it.
* For once spaces don’t matter: ((2+3)) will work the same as (( 2+3 )) and (( 2 + 3 )).
* Bash only operates with integers. Trying to do something like this (( 5 / 2 )) will give you “2”, and trying to do something like this (( 2.5 & 7 )) will result in an error. Then again, using anything but integers in a bitwise operation (which is what we are talking about now) is generally something you wouldn’t do anyway.

.. tip:: If you want to check what your decimal number would look like in binary, you can use bc, the command-line calculator that comes preinstalled with most Linux distros. For example, using::

        bc <<< "obase=2; 97"

    will convert 97 to binary (the o in obase stands for output), and …

        bc <<< "ibase=2; 11001011"

    will convert 11001011 to decimal (the i in ibase stands for input).

&& is a Logical Operator
---------------------------
Although it uses the same logic principles as its bitwise cousin, Bash’s && operator can only render two results: 1 (“true”) and 0 (“false”). For Bash, any number not 0 is “true” and anything that equals 0 is “false.” What is also false is anything that is not a number::

    $ echo $(( 4 && 5 )) # Both non-zero numbers, both true = true
    1

    $ echo $(( 0 && 5 )) #  One zero number, one is false = false
    0

    $ echo $(( b && 5 )) #  One of them is not number, one is false = false
    0

The OR counterpart for && is || and works exactly as you would expect.

All of this is simple enough… until it comes to a command’s exit status.

&& is a Logical Operator for Command Exit Status
--------------------------------------------------
As a command runs, it outputs error messages. But, more importantly for today’s discussion, it also outputs a number when it ends. This number is called an exit code, and if it is 0, it means the command did not encounter any problem during its execution. If it is any other number, it means something, somewhere, went wrong, even if the command completed.

So 0 is good, any other number is bad, and, in the context of exit codes, 0/good means “true” and everything else means “false.” Yes, this is the exact contrary of what you saw in the logical operations above, but what are you gonna do? Different contexts, different rules. The usefulness of this will become apparent soon enough.

Try it with::

    $ find /etc -iname "*.service"
    echo $?

**Important examples:**

Any of the commands fails, the following commands **will still run**::

    configure; make; make install

Any of the commands fails, the following commands will **not** run::

    configure && make && make install

This takes the exit code from each command and uses it as an operand in a chained && operation.

You can do something similar with ||, the OR logical operator, and make Bash continue processing chained commands if only one of a pair completes.

::

    mkdir test_dir 2>/dev/null || touch backup/dir/images.txt && find . -iname "*jpg" > backup/dir/images.txt &
    
So, assuming you are running the above from a directory for which you have read and write privileges, what it does it do and how does it do it? How does it avoid unseemly and potentially execution-breaking errors? Next week, apart from giving you the solution, we’ll be dealing with brackets: curly, curvy and straight. Don’t miss it!