.. _bash-examples:

Bash examples
==============
Store multiple lines into an variable
----------------------------------------
Use **here-document** to store multiple lines into the ``text`` variable:

.. literalinclude:: code/bash/ex-multilines.sh
    :language: bash
    :emphasize-lines: 1,3-5
    :linenos:

.. note:: On the last line ``echo "$text"``, $text should be included in quotes.

Output::

    bash$ ./ex-multilines.sh 
    line 1
    line 2
        some other text
        more text

    ***use read***

    line 1 from read
    line 2
        some other text
        more text

The here-document form of <<- means all leading hard tabs are removed from the input, so you must use tabs to indent your text. Quotes around "EOT" prevent shell expansion features, so the input is used verbatim. 

.. tip:: With ``-e`` option, ``echo -e`` will interpret ``\n, \t, \v`` etc. in the output. In this way, you can store multiple lines into a variable as well:

    .. literalinclude:: code/bash/ex-multilines2.sh
        :language: bash
        :emphasize-lines: 13-21,36-37
        :linenos:

    Output::

        <?php 
        $table_prefix = 'wp_'

        define('DB_HOST', 'myHost.com')
        define('DB_NAME', 'myDB')
        define('DB_USER', 'george')
        define('DB_PASSWORD', 'secret')
        define('DB_CHARSET', 'utf8')


        if (isset(['HTTP_X_FORWARDED_PROTO']) && ['HTTP_X_FORWARDED_PROTO'] === 'https') {
            ['HTTPS'] = 'on';
        }

        if (!defined('ABSPATH')) {
            define('ABSPATH', dirname(__FILE__) . '/');
        }

        require_once(ABSPATH . 'wp-secrets.php');

        require_once(ABSPATH . 'wp-settings.php');    

backup.sh
---------------

.. literalinclude:: code/bash/backup.sh
    :language: bash
    :emphasize-lines: 1,3-5
    :linenos:
