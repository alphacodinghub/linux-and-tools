.. _curly-brackets:

Curly Brackets in bash

**Ref**:
`All about {Curly Braces} in Bash <https://www.linux.com/topic/desktop/all-about-curly-braces-bash/>`_

Curly Brackets ``{}`` in Bash
-------------------------------
**Array Builder**
^^^^^^^^^^^^^^^^^^^

.. code-block:: bash
    :emphasize-lines: 7,12
    :linenos:

    bash$ echo {0..10}
    0 1 2 3 4 5 6 7 8 9 10
    bash$ echo {10..0..2}
    10 8 6 4 2 0
    bash$ echo {z..b..2}
    z x v t r p n l j h f d b
    bash$ echo {z..w..2}{001..004}
    z001 z002 z003 z004 x001 x002 x003 x004
    bash$ month=("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")
    bash$ echo ${month[3]} # Array indexes start at [0], so [3] points to the fourth item
    Apr
    bash$ letter_combos=({a..z}{a..z})

You can also do this::

    dec2bin=({0..1}{0..1}{0..1}{0..1}{0..1}{0..1}{0..1}{0..1})

This last one is particularly interesting because dec2bin now contains all the binary numbers for an 8-bit register, in ascending order, starting with 00000000, 00000001, 00000010, etc., until reaching 11111111. You can use this to build yourself an 8-bit decimal-to-binary converter. Say you want to know what 25 is in binary. You can do this::

    $ echo ${dec2bin[25]}
    00011001

**Parameter expansion**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    echo ${month[3]}

Here the braces ``{}`` are not being used as apart of a sequence builder, but as a way of generating parameter expansion. Parameter expansion involves what it says on the box: it takes the variable or expression within the braces and expands it to whatever it represents.

Suppose you have a variable like::

    a="Too longgg"

The command::

    echo ${a%gg}

chops off the last two gs and prints “Too long“.

Breaking this down,

* ${...} tells the shell to expand whatever is inside it
* a is the variable you are working with
* % tells the shell you want to chop something off the end of the expanded variable (“Too longgg”), and gg is what you want to chop off.
* If you need to chop off a chunk from the beginning of a variable, instead of %, use #::

    $ a="Hello World!"

    $ echo Goodbye${a#Hello}

    Goodbye World!

``ImageMagick`` is a set of command line tools that lets you manipulate and modify images. One of its most useful tools ImageMagick comes with is convert. In its simplest form convert allows you to, given an image in a certain format, make a copy of it in another format.

The following command takes a JPEG image called image.jpg and creates a PNG copy called image.png::

    convert image.jpg image.png

ImageMagick is often pre-installed on most Linux distros. If you can’t find it, look for it in your distro’s software manager.

Okay, end of digression. On to the example:

With variable expansion, you can do the same as shown above like this::

    i=image.jpg
    convert $i ${i%jpg}png

What you are doing here is chopping off the extension jpg from i and then adding png, making the command convert image.jpg image.png.

You may be wondering how this is more useful than just writing in the name of the file. Well, when you have a directory containing hundreds of JPEG images, you need to convert to PNG, run the following in it::

    for i in *.jpg; do convert $i ${i%jpg}png; done

… and, hey presto! All the pictures get converted automatically.

**Output Grouping**
^^^^^^^^^^^^^^^^^^^^

Meanwhile, let’s finish up with something simple: you can also use { ... } to group the output from several commands into one big blob. The command::

    { echo "I found all these PNGs:"; find . -iname "*.png"; echo "Within this bunch of files:"; ls; } > PNGs.txt

creates the file PNGs.txt with everything, starting with the line “I found all these PNGs:“, then the list of PNG files returned by find, then the line “Within this bunch of files:” and finishing up with the complete list of files and directories within the current directory.

.. important:: 
    Notice that **there is space between the braces** and the commands enclosed within them. That’s because { and } are reserved words here, commands built into the shell. They would roughly translate to “group the outputs of all these commands together” in plain English.

    Also notice that the list of commands has to end with a semicolon (;) or the whole thing will bork.