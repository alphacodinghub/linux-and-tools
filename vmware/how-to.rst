.. _vmware-how-to:

VMware How TO
==============
Git permision problems with shared folders on Vmware MACOS guest
------------------------------------------------------------------
VMware on Windows machine::

    Host machine OS: Windows
    Guest machine OS: MACOS

On a vmware MACOS guest, if you git repositories on shared folders you may run into permision problems::

    ➜ git clone git@gitlab.com:alphacodinghub/astroplan.git test
    Cloning into 'test'...
    remote: Enumerating objects: 5574, done.
    fatal: Unable to create temporary file '/Volumes/VMware Shared Folders/D/tmp/test/.git/objects/pack/tmp_pack_XXXXXX': Permission denied
    fatal: index-pack failed

To solve this problem, use smb to connect to shared folders.

**How to connect:**

On your Mac, from Finder's ``Go`` menu, choose ``Connect to Server``. Enter the IP address of your host PC, preceded by ``smb://``, as shown:

.. image:: _static/01.png

.. tip:: You can use your PC's name instead of the IP address, e.g. ``smb://mypcname``.

You will be prompted for network credentials - by default, your Windows username and password.

**Mounted folder:**

Once connected, you will fine it in /Volumes/SharedFolder. You will be able to git in the mounted shared folder.

.. tip:: You can create a link to your home folder so that you can enter the shared folder from you home folder:

    ::
    
        ln -s /Volumes/shared-folder-name   ~/shared-folder-name


