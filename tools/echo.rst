.. _echo:

ECHO command examples
======================
**echo** command in linux is used to display line of text/string that are passed as an argument . This is a built in command that is mostly used in ``shell`` scripts and batch files to output status text to the screen or a file.

**Syntax**:

.. code-block:: bash

    echo [option] [string]

examples:

.. code-block:: bash

    (base) robert@ubuntu:~$ echo "Hello World!"
    Hello World!
    (base) robert@ubuntu:~$ echo Hello World!
    Hello World!
    (base) robert@ubuntu:~$ echo Hello "World!"
    Hello World!

**echo Options** 

.. csv-table:: echo Options
    :header: Options, Description
    :widths: 10, 70

    -n,	 "do not print the trailing newline."
    -e,	 "enable interpretation of backslash escapes."
    \b,	 "backspace - back one space"
    \\ \\,	 backslash
    \\n,	 "new line"
    \\r,	 "carriage return"
    \\t,	 horizontal tab
    \\v,	 vertical tab
    \\c,    suppress trailing new line

.. note:: The most useful option is ``-e``, which enables the interpretation of backslash escapes.

Examples:

.. code-block:: bash

    (base) robert@ubuntu:~$ echo -e "Hello \bWorld!"
    HelloWorld!
    (base) robert@ubuntu:~$ echo -e "Hello \nWorld!"
    Hello 
    World!
    (base) robert@ubuntu:~$ echo -e "\tHello \n\tWorld!"
        Hello 
        World!
    (base) robert@ubuntu:~$ echo -e "\tHello \vWorld!"
        Hello 
                World!
    (base) robert@ubuntu:~$ echo -e "Hello World! \c"
    Hello World! (base) robert@ubuntu:~$ 

``echo *`` : this command will print all files/folders, similar to ls command.

.. code-block:: bash

    (base) robert@ubuntu:~$ echo *
    Desktop Documents Downloads hk-150.109.122.204.sh miniconda3 Music Pictures Public shared snap Templates Videos
    (base) robert@ubuntu:~$ echo Do*
    Documents Downloads
    (base) robert@ubuntu:~$ echo *.sh
    hk-150.109.122.204.sh
    (base) robert@ubuntu:~$ 

The echo can be used with redirect operator to output to a file and not standard output.

.. code-block:: bash

    (base) robert@ubuntu:~$ echo *.sh "hello world"
    hk-150.109.122.204.sh hello world
    (base) robert@ubuntu:~$ echo *.sh "hello world" > output.txt
    (base) robert@ubuntu:~$ cat output.txt 
    hk-150.109.122.204.sh hello world
    (base) robert@ubuntu:~$ 

More examples:

.. code-block:: bash

    (base) robert@ubuntu:~$ str=" Files in the current folder:\n * "
    (base) robert@ubuntu:~$ echo str
    str
    (base) robert@ubuntu:~$ echo $str
    Files in the current folder:\n Desktop Documents Downloads hk-150.109.122.204.sh miniconda3 Music output.txt Pictures Public shared snap Templates Videos
    (base) robert@ubuntu:~$ echo -e $str
    Files in the current folder:
    Desktop Documents Downloads hk-150.109.122.204.sh miniconda3 Music output.txt Pictures Public shared snap Templates Videos
    (base) robert@ubuntu:~$ 
