.. _rest_toc:

:ref:`rst-how-to-overview`

Directive: `toctree`
======================
The toctree `directive` allows you to insert other files within a RST file. 
The reason to use this directive is that RST does not have facilities to interconnect several documents, or split documents into multiple output files. The `toctree` directive looks like::

    .. toctree::
        :maxdepth: 3
        :numbered: 2
        :titlesonly:
        :glob:
        :hidden:

        index.rst
        chapter1.rst
        chapter2.rst

It includes 3 RST files and shows a TOC that includes the title found in the RST documents.

Here are a few notes about the different options:

* maxdepth: is used to indicates the depth of the tree.
* numbered: adds relevant section numbers.
* titlesonly: adds only the main title of each document
* glob: can be used to indicate that * and ? characters are used to indicate patterns.
* hidden: hides the toctree. It can be used to include files that do not need to be shown (e.g. a bibliography).

The **glob** option works as follows::

    .. toctree::
        :glob:

        chapter?
        recipe/*
        *

.. note:: Without `glob` directive, file patterns with "*" and "?" won't work.

**Refs**:
* `reStructuredText Directives <https://docutils.sourceforge.io/docs/ref/rst/directives.html>`_
* `Restructured Text (reST) and Sphinx CheatSheet <https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html>`_
* `reST Directives <https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html>`_
