.. _restRaw:

`raw` Directive
================
To insert HTML markups into the `rst` file
-------------------------------------------------
::

    .. raw:: html

        <a href="https://google.co.uk">Click to Google</a>

This will show:

.. raw:: html

    <a href="https://google.co.uk" target="_blank">Click to Google</a>


Embed external html file using iframe
--------------------------------------

If you embed an interactive graph, which is saved as an html file, in a reStructuredText 
document using iframe::

    .. raw:: html

        <iframe src="_static/filename.html" height="345px" width="100%"></iframe>

.. note:: The embeded html file should be put in the `_static` folder. It will then be copied into 
    `build/html/_static` folder when being rendered into html.

The other way may like this::

    .. raw:: html
        :file: filename.html


