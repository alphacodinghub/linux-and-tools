.. _macos-how-to:

MACOS How to
==============
Add users to "wheel" group
-------------------------------
The command to add group wheel::

    $ sudo dscl . append /Groups/wheel GroupMembership $USER

Where "$USER" refers to your current username althouth you can replace it with an actual ``username``. Then re-run "id $USER" to see that you have indeed been added to group wheel, like so::

    (base) ➜  ~ id $USER
    uid=501(robert) gid=20(staff) groups=20(staff),0(wheel),12(everyone),61(localaccounts),79(_appserverusr),80(admin),81(_appserveradm),98(_lpadmin),701(com.apple.sharepoint.group.1),33(_appstore),100(_lpoperator),204(_developer),250(_analyticsusers),395(com.apple.access_ftp),398(com.apple.access_screensharing),399(com.apple.access_ssh),400(com.apple.access_remote_ae)
    (base) ➜  ~

"macOS cannot verify that this app is free from malware."
-----------------------------------------------------------
“chromedriver” cannot be opened because the developer cannot be verified". "macOS cannot verify that this app is free from malware."

https://stackoverflow.com/questions/60362018/macos-catalinav-10-15-3-error-chromedriver-cannot-be-opened-because-the-de

I found the work around as below:

1. Open terminal
#. Navigate to path where your chromedriver file is located
#. Execute any one of the below commands

**Command1**: xattr -d com.apple.quarantine <name-of-executable>

Example

/usr/local/Caskroom/chromedriver 
$ xattr -d com.apple.quarantine chromedriver 

(or)

**Command2**: spctl --add --label 'Approved' <name-of-executable>

Source: https://docwhat.org/upgrading-to-catalina

Note: This will work only with the file(s) where the above command is executed. If a new chromedriver is downloaded then the co