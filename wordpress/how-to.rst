.. _how-to:

WordPress How-to
=================

H5P: cURL error 28: Operation timed out after 30000 milliseconds with...
--------------------------------------------------------------------------
It is no use to increase the following parameters in php.ini::

    max_execution_time = 600
    max_input_time = 1000
    default_socket_timeout = 600

The right solution is to increase the curl timeout settings. To add the below code into themes function.php::

    // Setting a custom timeout value for cURL. 
    add_action('http_api_curl', 'sar_custom_curl_timeout', 9999, 1);
    function sar_custom_curl_timeout( $handle ){
        curl_setopt( $handle, CURLOPT_CONNECTTIMEOUT, 300 ); // 300 seconds. Too much for production, only for testing.
        curl_setopt( $handle, CURLOPT_TIMEOUT, 300 ); // 300 seconds. Too much for production, only for testing.
    }

.. tip:: The unit of TIMEOUT is second.

To hide login and wp-admin page
-------------------------------------
Use plugin: Webcraftic Hide login page

How to configure email service
----------------------------------
# email plugins

    A. WP Mail SMTP by WPForms
    #. Easy WP SMTP

        when configuring office365, email server may reject the authentication and you will receive an email notification saying suspicious logins detected. You will need to to click ``Review recent activity`` link to confirm "it was me" to allow your wordpress site to use your email to send emails.

        office365 SMTP: smtp.office365.com, port: 587, encription method: STARTTLS
