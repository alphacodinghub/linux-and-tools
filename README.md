# Linux and Tools

## Tips

It is recommended that you log into the docker container, check the container's IP address, run the auto-build command as follows:

```
$ docker run --rm -it -v $PWD:/docs alphacodinghub/sphinx-latexpdf bash
$ ip address show
$ sphinx-autobuild  .  _build/html -H 0.0.0.0
```

You can then live view the document output by accessing to the container:

```
$ http://<container's IP address>:8000
```

If on a MAC:

```
$ docker run --rm -it -p 8000:8000 -v $PWD:/docs alphacodinghub/sphinx-latexpdf sphinx-autobuild  .  _build/html -H 0.0.0.0
# then on the host machine (MAC):
http://localhost:8000
```
