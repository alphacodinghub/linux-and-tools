.. Linux and Tools documentation master file, created by
   sphinx-quickstart on Sat Aug 15 21:16:05 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive. 
   可以使用中文的。

Welcome to Linux and Tools's Documentation!
===========================================

.. toctree::
   :maxdepth: 3
   :numbered: 2
   :glob: 
   :caption: Contents:

   */index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
