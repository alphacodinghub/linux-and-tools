.. _phpRegExpr:

PHP Regular Expressions: Preg_match, Preg_split, Preg_replace
=================================================================
**PHP REGULAR EXPRESSION** also known as regex are powerful pattern matching algorithm that can be performed in a single expression. Regular expressions use arithmetic operators such as (+,-,^) to create complex expressions. They can help you accomplish tasks such as validating email addresses, IP address etc.

Built-in Regular expression Functions in PHP
-----------------------------------------------
* preg_match – this function is used to perform a pattern match on a string. It returns true if a match is found and false if a match is not found.
* preg_split – this function is used to perform a pattern match on a string and then split the results into a numeric array
* preg_replace – this function is used to perform a pattern match on a string and then replace the match with the specified text.

**Syntax**::

    preg_match(pattern, input, matches, flags, offset)

**Parameter Values**

=========   ====================================================================================================================================================================================================================================================================================================
Parameter	Description
pattern	    Required. Contains a regular expression indicating what to search for
input	    Required. The string in which the search will be performed
matches	    Optional. The variable used in this parameter will be populated with an array containing all of the matches that were found
flags	    Optional. A set of options that change how the matches array is structured:
            * PREG_OFFSET_CAPTURE - When this option is enabled, each match, instead of being a string, will be an array where the first element is a substring containing the match and the second element is the position of the first character of the substring in the input.
            * PREG_UNMATCHED_AS_NULL - When this option is enabled, unmatched subpatterns will be returned as NULL instead of as an empty string.
offset	    Optional. Defaults to 0. Indicates how far into the string to begin searching. The preg_match() function will not find matches that occur before the position given in this parameter
=========   ====================================================================================================================================================================================================================================================================================================

**Return Value**:	Returns 1 if a match was found, 0 if no matches were found and false if an error occurred

Example - preg_match:

.. code-block:: php
    :linenos:

    <?php
    $my_url = "www.guru99.com";
    if (preg_match("/guru/", $my_url))
    {
        echo "the url $my_url contains guru";
    }
    else
    {
        echo "the url $my_url does not contain guru";
    }
    ?>

Another example:

.. code-block:: php
    :linenos:

    <?php
	public function cms_db_wordpress($cmsPath){
		$dbFile=realpath($cmsPath.'/wp-config.php');
		$configTxt=file_get_contents($dbFile);
		$cmsDb=array(
			'db_user'  => 'DB_USER',
			'db_pwd'   => 'DB_PASSWORD',
			'db_host'  => 'DB_HOST',
			'db_name'  => 'DB_NAME',
			'db_charset'  => 'DB_CHARSET',
		);
		//匹配定义的参数
		foreach ($cmsDb as $dbKey=>$dbDefine){
			if(preg_match('/define\s*\(\s*[\'\"]\s*'.$dbDefine.'\s*[\'\"]\s*\,\s*[\'\"](?P<val>[^\'\"]*?)[\'\"]\s*\)/i', $configTxt,$dbDefineVal)){
				$cmsDb[$dbKey]=$dbDefineVal['val'];
			}else{
				// $cmsDb[$dbKey]='';
				$cmsDb[$dbKey]= getenv($dbDefine) ?: '';
			}
		}
		//匹配表前缀
		if(preg_match('/\$table_prefix\s*=\s*[\'\"](?P<val>[^\'\"]*?)[\'\"]/i', $configTxt,$tablePre)){
			$cmsDb['db_prefix']=$tablePre['val'];
		}else{
			// $cmsDb['db_prefix']='';
			$cmsDb['db_prefix']=getenv('TABLE_PREFIX') ?: 'wp_';
		}
		$cmsDb['db_type']='mysql';
		$cmsDb['db_port']=3306;
		return $cmsDb;
	}

Example - Preg_split:

.. code-block:: php
    :linenos:

    <?php

    $my_text="I Love Regular Expressions";

    $my_array  = preg_split("/ /", $my_text);

    print_r($my_array );

    ?>

Example - Preg_replace:

.. code-block:: php
    :linenos:

    <?php

    $text = "We at Guru99 strive to make quality education affordable to the masses. Guru99.com";

    $text = preg_replace("/Guru/", '<span style="background:yellow">Guru</span>', $text);

    echo $text;

    ?>

**Cheat Sheet**

======== ============================================= =====    =================================================
[abc]	 A single character of: a, b or c               ^	    Start of line
[^abc]	 Any single character except: a, b, or c        $	    End of line
[a-z]	 Any single character in the range a-z          \\A	    Start of string
[a-zA-Z] Any single character in the range a-z or A-Z   \\z	    End of string
(...)	 Capture everything enclosed                    .	    Any single character
(a|b)	 a or b                                         \\s	    Any whitespace character
a?       Zero or one of a                               \\S	    Any non-whitespace character
a*       Zero or more of a                              \\d	    Any digit
a+       One or more of a                               \\D	    Any non-digit
a{3}	 Exactly 3 of a                                 \\w	    Any word character (letter, number, underscore)
a{3,}	 3 or more of a                                 \\W	    Any non-word character
a{3,6}	 Between 3 and 6 of a                           \\b	    Any word boundary
======== ============================================= =====    =================================================


**Refs**:
---------------
* `PHP Regular Expression Functions <https://www.w3schools.com/php/php_ref_regex.asp>`_
* `PHP Regular Expressions Tutorial <https://www.guru99.com/php-regular-expressions.html>`_
* `Live Regex <https://www.phpliveregex.com/#tab-preg-match>`_