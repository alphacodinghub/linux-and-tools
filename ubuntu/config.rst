.. _ubuntu-config:

Configure Ununtu
=================
Working with Terminal - Zsh & oh My Zsh
-----------------------------------------
Install zsh::

    sudo apt install zsh

Set as default shell::

    chsh -s $(which zsh)

Logout then login again. Open a terminal and choose option 2::

    This is the Z Shell configuration function for new users,
    zsh-newuser-install.
    You are seeing this message because you have no zsh startup files
    (the files .zshenv, .zprofile, .zshrc, .zlogin in the directory
    ~).  This function can help you with a few settings that should
    make your use of the shell easier.

    You can:

    (q)  Quit and do nothing.  The function will be run again next time.

    (0)  Exit, creating the file ~/.zshrc containing just a comment.
        That will prevent this function being run again.

    (1)  Continue to the main menu.

    (2)  Populate your ~/.zshrc with the configuration recommended
        by the system administrator and exit (you will need to edit
        the file by hand, if so desired).

    --- Type one of the keys in parentheses --- 2
    /home/robert/.zshrc:15: scalar parameter HISTFILE created globally in function zsh-newuser-install
    (eval):1: scalar parameter LS_COLORS created globally in function zsh-newuser-install

Install oh My Zsh::

    ~ % sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

    Cloning Oh My Zsh...
    Cloning into '/home/robert/.oh-my-zsh'...
    remote: Enumerating objects: 1158, done.
    remote: Counting objects: 100% (1158/1158), done.
    remote: Compressing objects: 100% (1127/1127), done.
    remote: Total 1158 (delta 20), reused 1059 (delta 15), pack-reused 0
    Receiving objects: 100% (1158/1158), 778.12 KiB | 1.83 MiB/s, done.
    Resolving deltas: 100% (20/20), done.

    Looking for an existing zsh config...
    Found ~/.zshrc. Backing up to /home/robert/.zshrc.pre-oh-my-zsh
    Using the Oh My Zsh template file and adding it to ~/.zshrc.

            __                                     __
    ____  / /_     ____ ___  __  __   ____  _____/ /_
    / __ \/ __ \   / __ `__ \/ / / /  /_  / / ___/ __ \
    / /_/ / / / /  / / / / / / /_/ /    / /_(__  ) / / /
    \____/_/ /_/  /_/ /_/ /_/\__, /    /___/____/_/ /_/
                            /____/                       ....is now installed!


    Before you scream Oh My Zsh! please look over the ~/.zshrc file to select plugins, themes, and options.

    • Follow us on Twitter: https://twitter.com/ohmyzsh
    • Join our Discord server: https://discord.gg/ohmyzsh
    • Get stickers, shirts, coffee mugs and other swag: https://shop.planetargon.com/collections/oh-my-zsh

Install plugin "zsh-autosuggestions"::

    git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

Add the plugin in to the configure file `~/.zshrc`::

    # Which plugins would you like to load?
    # Standard plugins can be found in $ZSH/plugins/
    # Custom plugins may be added to $ZSH_CUSTOM/plugins/
    # Example format: plugins=(rails git textmate ruby lighthouse)
    # Add wisely, as too many plugins slow down shell startup.
    plugins=(git zsh-autosuggestions)

Edit `~/.zshrc` to enable auto-correction and add aliases::

    # Uncomment the following line to enable command auto-correction.
    ENABLE_CORRECTION="true"


    # Set personal aliases, overriding those provided by oh-my-zsh libs,
    # plugins, and themes. Aliases can be placed here, though oh-my-zsh
    # users are encouraged to define aliases within the ZSH_CUSTOM folder.
    # For a full list of active aliases, run `alias`.
    #
    # Example aliases
    # alias zshconfig="mate ~/.zshrc"
    alias zshconfig="vim ~/.zshrc"
    # alias ohmyzsh="mate ~/.oh-my-zsh"

Useful plugins::

    zsh-syntax-highlighting
    z
    


Refs
-----
* `How to Setup ZSH and Oh-my-zsh on Linux <https://www.howtoforge.com/tutorial/how-to-setup-zsh-and-oh-my-zsh-on-linux/>`_