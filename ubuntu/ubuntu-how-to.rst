.. _ubuntuHowTo:

How to
============================================
How to check installed packages on Ubuntu?
-------------------------------------------
To list all installed packages::

  $ apt list --installed
  ...
  yelp/focal,now 3.36.0-1 amd64 [installed,automatic]
  zenity-common/focal,focal,now 3.32.0-5 all [installed,automatic]
  zenity/focal,now 3.32.0-5 amd64 [installed,automatic]
  zip/focal,now 3.0-11build1 amd64 [installed,automatic]
  zlib1g/focal,now 1:1.2.11.dfsg-2ubuntu1 amd64 [installed,automatic]

To list installed SPECIFIC packages::

  # to list packages which names start with `dock`:
  $ apt list --installed dock*
  docker-ce-cli/focal,now 5:19.03.12~3-0~ubuntu-focal amd64 [installed,automatic]
  docker-ce/focal,now 5:19.03.12~3-0~ubuntu-focal amd64 [installed]

To list available packages::

  $ apt list

To list available packages with filters::

  $ apt list *box
  python3-termbox/focal 1.1.2+dfsg-3build2 amd64
  rhythmbox/focal,now 3.4.4-1ubuntu2 amd64 [installed,automatic]
  ruby-rails-assets-jquery-colorbox/focal,focal 1.6.3~dfsg-5 all
  sbmltoolbox/focal,focal 4.1.0-4 all
  shellinabox/focal 2.21 amd64

We can see that `rhythmbox` has been installed already, others not.

Other examples::

  $ apt list | grep apache
  $ apt list | less
  $ apt list nginx*
  $ apt list pattern
  $ apt list --installed pattern

You can use `dpkg --list` as well::

  $ dpkg --list pattern
  $ dpkg -l pattern

How to completely remove a package?
------------------------------------
Use this command::

  sudo apt-get remove --purge <package-name>

e.g.::

  sudo apt-get remove --purge virtualbox

How to Install and Correct Dependencies Issues in Ubuntu
----------------------------------------------------------
If dependency issues happen when using `dpkg -i package-name` to install packages, 
you can try bellow command to install the dependencies::

    sudo apt-get -f install 

This will check the package dependencies and install them. You don't need to run `dpkg` again.

Ref
^^^^

1. `How to Install and Correct Dependencies Issues in Ubuntu <https://www.liquidweb.com/kb/how-to-install-dependencies-in-ubuntu/#:~:text=Install%20Dependencies,Tool)%20for%20software%20package%20management.&text=The%20main%20task%20of%20apt,packages%20along%20with%20their%20dependencies.>`_
#. `How to let *dpkg -i* install dependencies for me? <https://askubuntu.com/questions/40011/how-to-let-dpkg-i-install-dependencies-for-me>`_

To check groups that a user belongs to
-------------------------------------------
::

  id
  id -nG
  id  $user
  id username
  # add a user to a group
  sudo usermod -aG groupname username
  sudo usermod -aG docker robert   #add robert to docker group